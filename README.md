# Authenticator for Ironport Gateway

Same as firewal_auth script for fortigate gateway but this is for Ironport gateway.


Feel free to report bugs at https://github.com/krgaurav94/ironport_auth/.

## Usage
### Linux
Download 3 files: ironport_auth.py, ironport_proxy_auth.py, ironport_auth_dual.py

Run 
```
  $ python ironport_auth_dual.py
```
Install `requests` and other necessary python modules.

### Windows
Download installer from latest release and run the program from desktop shortcut.

TIP: Add username and password as target arguments in the desktop shortcut as shown below to avoid entering every time

![Screenshot](http://i.imgur.com/b8v0BiH.png)
